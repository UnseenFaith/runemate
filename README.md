# RuneMate

RuneMate is a program that makes setting runes less of a hassle in League of Legends. It uses outside sources, like [Champion.gg](https://champion.gg) and [KoreanBuilds](https://koreanbuilds.net), to get popular rune pages for the latest League patch. It's not meant to be a complete replacement for rune selection, but at least you will have a good idea of what you're expected to run on certain champions. 

Some Features of RuneMate:
- Made with performance in mind
- Works off of the League Client API itself
- Support for multiple sources
- No configuration needed or messy GUI's to work with. Just open it and you are good to go.

# How to Use
- Download latest program from the link below.
- Make sure you are logged into the league client and then open the program.
- Once you go into queue and select a champion, at the end of champion select, your rune page will automatically be added, and you will be able to edit it to your needs if you want.

## Couple of things to keep in mind:
- The program does not currently work when trading champions.
- The program tries to use KoreanBuilds first, and then defaults back to Champion.gg
- The program will delete rune pages it adds if the champion select is dodged or you finish your game.
- The program will not delete rune pages that are already there, and if you have a maximum amount of rune pages already set you will need to delete a page before using the program. (Might add Support for Auto-Deletion when Summoner has too many pages)


# Download

### You can download the latest version of the program from [here](https://gitlab.com/UnseenFaith/autoruneprogram/-/jobs/artifacts/master/raw/runemate.exe?job=build_and_deploy_exe).

# Suggestions/Feedback

If you have suggestions or feedback, you can leave a comment on the repository or message me on Discord: Faith#8243

