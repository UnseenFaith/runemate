process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

const App = require('./src/lib/app');

module.exports = {
    app: App,
    App,
    Page: require('./src/lib/structures/Page'),
    PageBuilder: require('./src/lib/structures/PageBuilder'),
    Routes: require('./src/lib/structures/Routes'),
    Source: require('./src/lib/structures/Source'),
    Summoner: require('./src/lib/structures/Summoner'),
    util: require('./src/lib/util/util'),
    Util: require('./src/lib/util/util'),
    Collection: require('./src/lib/util/Collection'),
    Colors: require('./src/lib/util/Colors'),
    RiotSocket: require('./src/lib/util/RiotSocket'),
    Stopwatch: require('./src/lib/util/Stopwatch'),
};

new App().login();