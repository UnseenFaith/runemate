// Our Main App Client which will hold everything during this apps lifetime.
const Summoner = require('./structures/Summoner');
const Collection = require('./util/Collection');
const Connector = require('lcu-connector');
const { sep } = require('path');
const { writeFileSync, watchFile } = require('fs');
const DEFAULT_CONFIG = require('./util/config.json');

let config;
let configPath = `${process.cwd()}${sep}config.json`;

try {
    config = require(configPath);
} catch (err) {
    console.log("No Default Config found. Making a new one in local directory.");
    config = DEFAULT_CONFIG;
    writeFileSync(configPath, JSON.stringify(DEFAULT_CONFIG));
}

watchFile(configPath, (current, previous) => {
    if (previous.mtime < current.mtime) {
        console.log("Config file changed. Reloading config file");
        delete require.cache[configPath];
        config = require(configPath);
    }
});

const promisify = require('util').promisify;
const readdir = promisify(require('fs').readdir);

const { resolve, dirname } = require('path');

class App {
    constructor() {
        this.connector = new Connector();

        this.summoner = new Summoner(this);

        this.sources = new Collection();

        this.settings = config;

        this.patch = null;

        this.connector.on('connect', this.connect.bind(this));
        this.connector.on('disconnect', this.disconnect.bind(this));
    }

    async login() {
        this.connector.start();
        await this.loadSources();
    }

    async loadSources() {
        const dir = resolve(dirname(require.main.filename), 'src', 'sources');
        const files = await readdir(dir);
        for (const file of files) {
            const source = new (require(resolve(dir, file)))(this.summoner);
            this.sources.set(source.name, source);
        }
        console.log(`Loaded ${this.sources.size} sources.`);
    }

    connect(details) {
        this.summoner.routes.patch(details);
    }

    disconnect() {
        process.exit(0);
    }
}

module.exports = App;