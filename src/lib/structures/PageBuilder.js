const Page = require('./Page');
const Collection = require('../util/Collection');

class PageBuilder {
    constructor(summoner) {
        Object.defineProperty(this, 'app', { value: summoner.app });
        Object.defineProperty(this, 'summoner', { value: summoner });

        this.runes = new Collection();

        this.styles = new Collection();

        this.id = null;

        this.set = false;

    }

    async create(champion, position) {
        try {
            const source = this.app.sources.get('KoreanBuilds');
            const page = await source.fetch(champion, position);
            const result = await this.summoner.routes.request('/lol-perks/v1/pages', { method: "POST", body: `${page}` }).then(r => r.json());
            this.id = result.id;
        } catch (error) {
            throw error;
        }
    }

    async initialize() {
        const [runes, styles] = await Promise.all([
            this.summoner.routes.request('/lol-perks/v1/perks').then(r => r.json()),
            this.summoner.routes.request('/lol-perks/v1/styles').then(r => r.json())
        ]);

        for (const rune of runes) this.runes.set(rune.id, rune);
        for (const style of styles) this.styles.set(style.id, style);

        console.log("PageBuilder Initialized");
    }

}

module.exports = PageBuilder;