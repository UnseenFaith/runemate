const PageBuilder = require('./PageBuilder');
const Routes = require('./Routes');

class Summoner {
    constructor(app) {
        Object.defineProperty(this, 'app', { value: app });

        this.id = null;

        this.routes = new Routes(this);

        this.pages = new PageBuilder(this);
    }

    async initialize() {
        this.id = await this.routes.request('/lol-summoner/v1/current-summoner')
            .then(r => r.json())
            .then(json => json.summonerId);
        if (this.app.settings.makeRoom) {
            const [pages, count] = await Promise.all([
                this.routes.request('/lol-perks/v1/pages').then(r => r.json()).then(json => json.filter(p => p.isDeletable)),
                this.routes.request('/lol-perks/v1/inventory').then(r => r.json()).then(json => json.ownedPageCount),
            ]);
            if (pages.length >= count) {
                console.log("Deleting a page to make room for new pages.");
                const pageID = pages[pages.length - 1].id;
                this.routes.request(`/lol-perks/v1/pages/${pageID}`, { method: "DELETE" });
            }
        }
        console.log("Summoner Initialized");
    }
}

module.exports = Summoner;