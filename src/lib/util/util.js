const fetch = require('node-fetch');

class Util {
    static fetchText(...args) {
        return fetch(...args).then(res => res.text());
    }

    static strip($, element) {
        return $(element).text().toLowerCase();
    }
}

const Names = [
    "PRECISION",
    "SORCERY",
    "DOMINATION",
    "RESOLVE",
    "INSPIRATION",
    "PRESS THE ATTACK",
    "LETHAL TEMPO",
    "FLEET FOOTWORK",
    "CONQUEROR",
    "OVERHEAL",
    "TRIUMPH",
    "PRESENCE OF MIND",
    "LEGEND: ALACRITY",
    "LEGEND: TENACITY",
    "LEGEND: BLOODLINE",
    "COUP DE GRACE",
    "CUT DOWN",
    "LAST STAND",
    "ELECTROCUTE",
    "PREDATOR",
    "DARK HARVEST",
    "HAIL OF BLADES",
    "CHEAP SHOT",
    "TASTE OF BLOOD",
    "SUDDEN IMPACT",
    "ZOMBIE WARD",
    "GHOST PORO",
    "EYEBALL COLLECTION",
    "RAVENOUS HUNTER",
    "INGENIOUS HUNTER",
    "RELENTLESS HUNTER",
    "ULTIMATE HUNTER",
    "SUMMON AERY",
    "ARCANE COMET",
    "PHASE RUSH",
    "NULLIFYING ORB",
    "MANAFLOW BAND",
    "NIMBUS CLOAK",
    "TRANSCENDENCE",
    "CELERITY",
    "ABSOLUTE FOCUS",
    "SCORCH",
    "WATERWALKING",
    "GATHERING STORM",
    "GRASP OF THE UNDYING",
    "AFTERSHOCK",
    "GUARDIAN",
    "DEMOLISH",
    "FONT OF LIFE",
    "SHIELD BASH",
    "CONDITIONING",
    "SECOND WIND",
    "BONE PLATING",
    "OVERGROWTH",
    "REVITALIZE",
    "UNFLINCHING",
    "GLACIAL AUGMENT",
    "KLEPTOMANCY",
    "UNSEALED SPELLBOOK",
    "HEXTECH FLASHTRAPTION",
    "MAGICAL FOOTWEAR",
    "PERFECT TIMING",
    "FUTURE'S MARKET",
    "MINION DEMATERIALIZER",
    "BISCUIT DELIVERY",
    "COSMIC INSIGHT",
    "APPROACH VELOCITY",
    "TIME WARP TONIC"
];

// Set our Longest name
Util.longest = Names.reduce((long, str) => Math.max(long, str.length), 0);

module.exports = Util;