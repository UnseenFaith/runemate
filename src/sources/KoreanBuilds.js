const Source = require('../lib/structures/Source');

class KoreanBuilds extends Source {
    constructor(summoner) {
        super(summoner, 'KoreanBuilds', 'https://koreanbuilds.net');

        // Order:
        // Primary-style
        // Sub-style
        // Primary Runes (4)
        // Sub Runes (2)
        // Flex (3)
        this.selectors = [
            "#reforged-primary > div.perk-style > div.perk-icon-wrap > div > img", 
            "#reforged-secondary > div.perk-style > div.perk-icon-wrap > div > img",
            "#reforged-primary > div:nth-child(3) > div.perk-icon-wrap > div > img",
            "#reforged-primary > div:nth-child(4) > div.perk-icon-wrap > div > img",
            "#reforged-primary > div:nth-child(5) > div.perk-icon-wrap > div > img",
            "#reforged-primary > div:nth-child(6) > div.perk-icon-wrap > div > img",
            "#reforged-secondary > div:nth-child(3) > div.perk-icon-wrap > div > img",
            "#reforged-secondary > div:nth-child(4) > div.perk-icon-wrap > div > img",
            "#wrapper > div:nth-child(4) > div.col-md-8 > div.statperk-row > div:nth-child(1) > img",
            "#wrapper > div:nth-child(4) > div.col-md-8 > div.statperk-row > div:nth-child(2) > img",
            "#wrapper > div:nth-child(4) > div.col-md-8 > div.statperk-row > div:nth-child(3) > img"
        ];

        // KoreanBuilds Styles and Perks are incorrectly labeled
        // So we hardcode them and make changes where its needed.
        this.styles = {
            '8000': 8000,
            '8100': 8100,
            '8200': 8200,
            '8300': 8400,
            '8400': 8300
        };

        this.slots = {
            '5001': 5001,
            '5002': 5002,
            '5003': 5008,
            '5005': 5005,
            '5007': 5007,
            '5008': 5003
        };

        // Lets us convert League positions into acceptable KoreanBuild positions
        this.positions = {
            "bottom": "Bot",
            "middle": "Mid",
            "utility": "Support",
            "top": "Top",
            "jungle": "Jungle"
        };
    }

    fetch(champion, role) {
        try {
            if (!role) {
                console.log("You are not in a role based queue, using Champion.GG");
                return this.summoner.app.sources.get('ChampionGG').fetch(champion);
            }
            return super.fetch(champion, role);
        } catch (error) {
            // console.error(error);
            return this.summoner.app.sources.get('ChampionGG').fetch(champion);
        }
    }

    parse($, [index, selector]) {
        const src = $(selector).attr('src');
        const id = src.match(/\d+/)[0];
        if ([0,1].includes(index)) return this.styles[id];
        else if ([8,9,10].includes(index)) return this.slots[id];
        return parseInt(id);
    }

    url(champion, role) {
        const patch = "9.7";
        return `${this.baseURL}/champion/${champion.name}/${role}/${patch}/enc/NA`;
    }
}

module.exports = KoreanBuilds;